# variables.tf

variable "hcp_client_id" {
  description = "Client ID to use for HCP authentication"
  type        = string
}

variable "hcp_client_secret" {
  description = "Client secret to use for HCP authentication"
  type        = string
  sensitive   = true
}

variable "hcp_organization_id" {
  description = "HCP Organization ID"
  type        = string
}

variable "hcp_project_id" {
  description = "HCP Project ID"
  type        = string
}

variable "hcloud_token" {
  description = "Token used for hcloud interaction"
  type        = string
  sensitive   = true
}

variable "hcloud_image_name" {
  description = "Name of image used for server installation"
  type        = string
}

variable "network_zone" {
  description = "ID for hetzner network zone to use for subnet"
  type        = string
}

variable "subnet_cidr" {
  description = "CIDR for subnet in which machines should be located"
  type        = string
}

variable "instance_count" {
  description = "Number of deployed instances"
  type        = number
  default     = 0
}

variable "location" {
  description = "Location for deployed instances"
  type        = string
}

variable "server_type" {
  description = "Server type to use for deployed instances"
  type        = string
}

variable "ansible_pull_repo" {
  description = "Checkout ref to use for ansible-pull"
  type        = string
}

variable "ansible_pull_playbook" {
  description = "Checkout ref to use for ansible-pull"
  type        = string
}

variable "ansible_pull_checkout" {
  description = "Checkout ref to use for ansible-pull"
  type        = string
}

variable "firewall_allow_cidrs" {
  description = "CIDRs to open firewall to"
  type        = list(any)
}

variable "ssh_authorized_keys" {
  description = "The authorized keys to use for root access"
  type        = map(any)
}

variable "docker_username" {
  description = "The docker username to use for registry cache login"
  type        = string
}
