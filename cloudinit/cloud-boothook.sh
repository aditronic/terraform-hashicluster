#!/bin/sh

echo proxy_sslverify=False >> /etc/dnf/dnf.conf

cd /etc/yum.repos.d
for i in rocky*.repo; do
  sed 's/^mirrorlist.*$//' $i | sed 's/^#baseurl/baseurl/' > /tmp/$i
  sed 's/dl.rockylinux.org/10.0.0.2:13128/' /tmp/$i > $i
  rm /tmp/$i
done
