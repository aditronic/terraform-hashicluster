# providers.tf

terraform {
  required_version = ">= 1.4"

  required_providers {
    cloudinit = { source = "hashicorp/cloudinit", version = ">= 2" }
    hcloud    = { source = "hetznercloud/hcloud", version = ">= 0.13" }
    hcp       = { source = "hashicorp/hcp", version = "~> 0.80" }
    random    = { source = "hashicorp/random", version = "~> 3.6" }
    vault     = { source = "hashicorp/vault", version = "~> 3.23" }
  }
}
