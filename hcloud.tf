provider "hcloud" {
  token = var.hcloud_token
}

data "hcloud_network" "internal" {
  name = "internal"
}

resource "hcloud_network_subnet" "install_network" {
  network_id   = data.hcloud_network.internal.id
  type         = "cloud"
  network_zone = var.network_zone
  ip_range     = var.subnet_cidr
}

data "hcloud_image" "install_image" {
  with_selector     = "name=${var.hcloud_image_name}"
  with_architecture = "arm"
}

resource "hcloud_ssh_key" "authorized_keys" {
  name       = keys(var.ssh_authorized_keys)[0]
  public_key = values(var.ssh_authorized_keys)[0]
}

data "template_file" "cloud_config" {
  template = file("cloudinit/cloud-config.yaml")

  vars = {
    hcloud_token        = var.hcloud_token
    ansible_repo        = var.ansible_pull_repo
    ansible_playbook    = var.ansible_pull_playbook
    ansible_checkout    = var.ansible_pull_checkout
    hcp_client_id       = var.hcp_client_id
    hcp_client_secret   = var.hcp_client_secret
    hcp_organization_id = var.hcp_organization_id
    hcp_project_id      = var.hcp_project_id
    docker_username     = var.docker_username
  }
}

data "cloudinit_config" "node" {
  gzip          = false
  base64_encode = false

  part {
    content_type = "text/cloud-config"
    filename     = "terraform.yaml"
    content      = data.template_file.cloud_config.rendered
  }
}

resource "random_pet" "cattle" {
  count = var.instance_count

  keepers = {
    image_id          = data.hcloud_image.install_image.id
    location          = var.location
    server_type       = var.server_type
    cloudinit_payload = data.cloudinit_config.node.rendered
  }
}

locals {
  subnet_list      = slice(split(".", var.subnet_cidr), 0, 3)
  subnet_ip_prefix = join(".", local.subnet_list)
}

resource "hcloud_server" "cluster_nodes" {
  count = var.instance_count

  name        = random_pet.cattle[count.index].id
  image       = random_pet.cattle[count.index].keepers.image_id
  server_type = random_pet.cattle[count.index].keepers.server_type
  location    = random_pet.cattle[count.index].keepers.location
  ssh_keys    = [hcloud_ssh_key.authorized_keys.id]
  user_data   = random_pet.cattle[count.index].keepers.cloudinit_payload

  public_net {
    ipv4_enabled = false
    ipv6_enabled = false
  }

  network {
    network_id = hcloud_network_subnet.install_network.network_id
    ip         = join(".", [local.subnet_ip_prefix, count.index + 1])
  }

  depends_on = [
    hcloud_network_subnet.install_network
  ]
}
